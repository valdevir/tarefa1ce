package ce;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Tarefa1 {
	// numero m�ximo de gera��es

	// Numero de Genes
	// static int numGenes = 8;

	boolean elitismo = false;
	public static Interface telaInterface;

	public static void main(String[] args) {
		// Cria a tela
		telaInterface = new Interface();
		telaInterface.setVisible(true);
	}

	/**
	 * M�todo seta par�metros externos ao problema.
	 */
	public void setaParametrosProblema() {
		Algoritimo algoritimo = new Algoritimo();
		// taxa de crossover de 70% - Conforme especificado exerc�cio.
		algoritimo.setTaxaDeCrossover(0.7);
		// taxa de muta��o de 0,125% que � 1/8 (8-> n�mero de bits).
		algoritimo.setTaxaDeMutacao(0.125);
		// elitismo
		// boolean eltismo = true;
		// tamanho da popula��o
		algoritimo.setTamPop(100);

		// Numero de Genes
		algoritimo.setNumGenes(8);
		// algoritimo.setMinimo(-10);
		// algoritimo.setMaximo(10);
	}

	public void executaAlgoritmoGenetico(int numGeracoes, int tamPopulacao,
			double taxaMutacao, double taxaCrossover) {
		try {

			Algoritimo algoritimo = new Algoritimo(numGeracoes, tamPopulacao,
					taxaMutacao, taxaCrossover);
			// Cria arquivo de log do algor�tmo.
			File file = new File("logGA.txt");
			// Cria stream de dados.
			OutputStream output = new FileOutputStream(file);
			// 1 passo � gerar a popula��o inicial.
			Populacao populacao = new Populacao(algoritimo.getTamPop(),
					algoritimo.getNumGenes(), true);
			StringBuilder concat = new StringBuilder("Individuos: \n");
			concat.append("Tamanho poupla��o = " + populacao.getTamPopulacao());
			concat.append("Gera��o " + 0 + ": \n");
			// Aplica fun��o Alpine
			algoritimo.aplicaFuncaoAlpine(populacao);
			// Aplica Fun��o de aptid�o e gera a aptid�o
			algoritimo.aplicaAptidaoProporcional(populacao);
			concat = geraLog(populacao, 0);
			// Gera log na tela.
			telaInterface.setLog(geraLogAreaRodada(populacao, 0));
			// System.out.println(concat + "\n" + "\n");
			int count = 0;
			while (count < concat.toString().length()) {
				output.write(concat.charAt(count));
				count++;
			}
			int geracao = 0;
			// loop at� crit�rio de parada
			while (geracao < algoritimo.getNumMaxGeracoes()) {
				geracao++;

				// Aplica Sele��o por roleta e gera popula��o intermedi�ria.
				Populacao populacaIntermediaria = algoritimo
						.selecaoRoleta(populacao);
				// Remove a popula��o selecionada.
				// Aplica Crossover.
				populacaIntermediaria = algoritimo
						.realizaCrossoverMutacao(populacaIntermediaria);
				for (int i = 0; i < populacaIntermediaria.getTamPopulacao(); i++) {
					populacao.getIndividuos().add(
							populacaIntermediaria.getIndividuos().get(i));
				}

				algoritimo.aplicaFuncaoAlpine(populacao);
				algoritimo.aplicaAptidaoProporcional(populacao);
				StringBuilder sb = geraLog(populacao, geracao);
				telaInterface.setLog(geraLogAreaRodada(populacao, geracao));
				// System.out.println(concat);
				// GraficoConvergencia
				telaInterface.getGraficoConvergencia().update(
						geracao,
						populacao,
						populacao.getIndividuos().get(0),
						populacao.getIndividuos().get(
								populacao.getTamPopulacao() - 1), 0);

				count = 0;
				while (count < sb.toString().length()) {
					output.write(sb.charAt(count));
					count++;
				}

			}
			output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block e.printStackTrace(); }

		}
	}

	private String geraLogAreaRodada(Populacao populacao, int geracao) {
		StringBuilder textoLog = new StringBuilder();
		textoLog.append("Gera��o: " + geracao + "\n");
		textoLog.append("Aptid�o Melhor Indiv�duo: "
				+ populacao.getIndividuos()
						.get(populacao.getTamPopulacao() - 1).getFitness()
				+ "\n");
		textoLog.append("Aptid�o Pior Indiv�duo : "
				+ populacao.getIndividuos().get(0).getFitness() + "\n");
		textoLog.append("Aptid�o M�dia: " + populacao.getMediaAptidao());
		textoLog.append("\n\n");

		return textoLog.toString();

	}

	/**
	 * Gera Arquivo de Log.
	 * 
	 * @param populacao
	 * @param geracao
	 * @return
	 */
	private static StringBuilder geraLog(Populacao populacao, int geracao) {
		StringBuilder concat = new StringBuilder();
		concat.append("\n Individuos Geracao " + geracao + "\n");
		concat.append("N - Bin X1  , Bin X2  , VlFObj   , Vl.X1Real, VlX2Real, Fitness\n");
		for (int i = 0; i < populacao.getNumIndividuos(); i++) {
			concat.append(i + " - ");
			concat.append(
					populacao.getIndividuos().get(i).getCromossomo()
							.getGeneX1String()).append(", ");
			concat.append(
					populacao.getIndividuos().get(i).getCromossomo()
							.getGeneX2String()).append(", ");
			concat.append(
					populacao.getIndividuos().get(i).getValorFuncaoObjetivo())
					.append(", ");
			concat.append(
					populacao.getIndividuos().get(i).getCromossomo()
							.getGeneX1Real()).append(", ");
			concat.append(
					populacao.getIndividuos().get(i).getCromossomo()
							.getGeneX2Real()).append(", ");
			concat.append(populacao.getIndividuos().get(i).getFitness());
			concat.append("\n");
		}
		return concat;
	}

	public static Interface getTelaInterface() {
		return telaInterface;
	}

	public static void setTelaInterface(Interface telaInterface) {
		Tarefa1.telaInterface = telaInterface;
	}
}
