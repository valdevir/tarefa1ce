package ce;

import java.util.ArrayList;

public class Populacao {

	private ArrayList<Individuo> individuos;

	@SuppressWarnings("unused")
	private int tamPopulacao;

	// Cria uma popula��o de indiv�duos aleat�rios.
	public Populacao(int tamPop, int numeroGenesByCromossomos,
			boolean isAleatorio) {
		if (isAleatorio) {
			tamPopulacao = tamPop;
			individuos = geraIndividuos(tamPop, numeroGenesByCromossomos);
		} else {
			individuos = new ArrayList<Individuo>();
		}
	}

	public Populacao() {
		individuos = new ArrayList<Individuo>();
	}

	/**
	 * Gera Indiv�duos aleatoriamente.
	 * 
	 * @param tamanhoPop
	 * @return
	 */
	private ArrayList<Individuo> geraIndividuos(int tamanhoPop,
			int numeroGenesByCromossomos) {
		ArrayList<Individuo> individuloList = new ArrayList<Individuo>();
		for (int i = 0; i < tamanhoPop; i++) {
			Individuo individuo = new Individuo(true, numeroGenesByCromossomos);
			individuloList.add(individuo);
		}
		return individuloList;

	}

	/**
	 * Faz a m�dia de aptid�o da popula��o.
	 * 
	 * @return
	 */
	public double getMediaAptidao() {
		double media = getSomaDaAptidao() / getNumIndividuos();
		return media;
	}

	/**
	 * Soma a aptid�o da popula��o.
	 * 
	 * @return
	 */
	public double getSomaDaAptidao() {
		double total = 0;
		for (int i = 0; i < individuos.size(); i++) {
			if (individuos.get(i) != null) {
				total += individuos.get(i).getFitness();
			}
		}
		return total;
	}

	/**
	 * Soma total dos valores da fun��o.
	 * 
	 * @return
	 */
	public double getSomaValorFuncao() {
		double total = 0;
		for (int i = 0; i < individuos.size(); i++) {
			if (individuos.get(i) != null) {
				total += individuos.get(i).getValorFuncaoObjetivo();
			}
		}
		return total;
	}

	// N�mero de indiv�duos existente na popula��o.
	public int getNumIndividuos() {

		return individuos.size();
	}

	public int getTamPopulacao() {
		return individuos.size();
	}

	public void setTamPopulacao(int tamPopulacao) {
		this.tamPopulacao = tamPopulacao;
	}

	public ArrayList<Individuo> getIndividuos() {
		return individuos;
	}

	public void setIndividuos(ArrayList<Individuo> individuos) {
		this.individuos = individuos;
	}

}
