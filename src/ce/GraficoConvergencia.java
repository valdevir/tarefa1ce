package ce;

import javax.swing.JPanel;


import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

/**
 * Classe que implementa o gr�fico com JFreeChart
 * @author valdevir
 *
 */
public class GraficoConvergencia {
	private JFreeChart jFreeChart;
	private XYSeriesCollection dataset = new XYSeriesCollection();
	XYSeries xySolucao;
	XYSeries xyMelhorDaGeracao;
	XYSeries xyPiorDaGeracao;
	XYSeries xyMediaDaGeracao;
	private ChartPanel chartPanel;
	private int height, 
	width;

	/**
	 * @param width
	 * @param height
	 */
	public GraficoConvergencia(int width, int height) {
		xySolucao = new XYSeries("Melhor Solu��o");
		xyMelhorDaGeracao = new XYSeries("Melhor da Gera��o");
		xyPiorDaGeracao = new XYSeries("Pior da Gera��o");
		xyMediaDaGeracao = new XYSeries("M�dia da Gera��o");
		this.height = height - 15;
		this.width = width;
		draw();
	}

	/**
	 * @param actualGeneration
	 * @param populacao
	 * @param piorIndividuo
	 * @param melhorIndividuo
	 * @param mediaFuncao
	 */
	public void update(int actualGeneration, Populacao populacao,
			Individuo piorIndividuo, Individuo melhorIndividuo,
			double minimoDaFuncao) {
		xySolucao.add(actualGeneration, minimoDaFuncao);
		xyMelhorDaGeracao.add(actualGeneration, melhorIndividuo.getFitness());
		xyPiorDaGeracao.add(actualGeneration, piorIndividuo.getFitness());
		xyMediaDaGeracao.add(actualGeneration, populacao.getMediaAptidao());
		jFreeChart.fireChartChanged();
	}

	public void clear() {
		xySolucao.clear();
		xyMelhorDaGeracao.clear();
		xyPiorDaGeracao.clear();
		xyMediaDaGeracao.clear();
	}

	public JPanel getImage() {
		return chartPanel;
	}

	private void draw() {
		jFreeChart = ChartFactory.createXYLineChart("Gr�fico de Converg�ncia",
				"Gera��es", "Fun��o Alpine", dataset, PlotOrientation.VERTICAL,
				true, true, false);
		chartPanel = new ChartPanel(jFreeChart);
		chartPanel.setPreferredSize(new java.awt.Dimension(width, height));
		chartPanel.setMinimumSize(new java.awt.Dimension(width, height));
		chartPanel.setMaximumSize(new java.awt.Dimension(width, height));
		chartPanel.setMouseZoomable(true);
		chartPanel.updateUI();
		dataset.addSeries(xyPiorDaGeracao);
		dataset.addSeries(xyMediaDaGeracao);
		dataset.addSeries(xyMelhorDaGeracao);
		dataset.addSeries(xySolucao);
	}
}
