package ce;

import java.util.Collections;
import java.util.Random;

public class Algoritimo {

	// taxa de crossover de 70% - Conforme especificado exerc�cio.
	private double taxaDeCrossoverDefault = 0.7;
	// taxa de muta��o de 0,125% que � 1/8 (8-> n�mero de bits).
	private double taxaDeMutacaoDefault = 0.125;
	// Numero de Genes
	private int numGenes = 8;
	private int numMaxGeracoesDefault = 200;
	// tamanho da popula��o
	private int tamPopDefault = 100;
	private static final int maximo = 10;
	private static final int minimo = -10;
	// Este ser�o os dados que o algor�tmo trabalhar�.
	private double taxaDeCrossover;
	private double taxaDeMutacao;
	private int tamPop;
	private int numMaxGeracoes;

	public Algoritimo(int numGeracoes, int tamPopulacao, double taxaMutacao,
			double taxaCrossover) {
		setNumMaxGeracoes(numGeracoes);
		setTamPop(tamPopulacao);
		setTaxaDeCrossover(taxaCrossover);
		setTaxaDeMutacao(taxaMutacao);
		// TODO Auto-generated constructor stub
	}

	public Algoritimo() {

	}

	/**
	 * Sele��o por roleta
	 * 
	 * @param populacao
	 */
	public Populacao selecaoRoleta(Populacao populacao) {
		float somaTotal = 0;
		Populacao populacaoAux = populacao;
		Populacao populacaIntermediaria = new Populacao(20, getNumGenes(),
				false);
		// Faz somta�rio de todos os fitness da popula��o.
		for (int i = 0; i < populacaoAux.getTamPopulacao(); i++) {
			somaTotal = somaTotal
					+ populacaoAux.getIndividuos().get(i).getFitness();
		}
		int count = 0;
		/*
		 * Arbitrariamente defini que a popula��o intermedi�ria poder� gerar
		 * filhos ser� de tamanho 20.
		 */
		while (count < 20) {
			count++;
			Random r = new Random();
			double soma = 0;
			int tamanhoPop = populacaoAux.getTamPopulacao();
			for (int i = 0; i < tamanhoPop; i++) {
				// Gera valor randomico de 0 at� a somat�ria dos fitness.
				double valorSorteado = r.nextDouble() * somaTotal;
				// Caso o valor encontrado maior que valor sorteado, escolhe
				// indiv�duo.
				if (soma >= valorSorteado) {
					populacaIntermediaria.getIndividuos().add(
							populacaoAux.getIndividuos().get(i));
					populacao.getIndividuos().remove(i);
					break;
				} else {
					soma = soma
							+ populacaoAux.getIndividuos().get(i).getFitness();
				}
			}
		}

		return populacaIntermediaria;
	}

	/**
	 * Aplica��o Fun��o Alpine
	 * 
	 * @param populacao
	 */
	public void aplicaFuncaoAlpine(Populacao populacao) {
		for (int i = 0; i < populacao.getTamPopulacao(); i++) {
			Individuo individuo = populacao.getIndividuos().get(i);
			float soma = 0;
			float x1 = individuo.getCromossomo().getGeneX1Real();
			float x2 = individuo.getCromossomo().getGeneX2Real();
			float valorX1 = (float) Math.abs(x1 * (Math.sin(x1)) + 0.1 * x1);
			float valorX2 = (float) Math.abs(x1 * (Math.sin(x1)) + 0.1 * x2);
			soma = valorX1 + valorX2;
			individuo.setValorFuncaoObjetivo(soma);
		}
	}

	/**
	 * Aplica a fun��o de aptid�o proporcional.
	 * 
	 * @param populacao
	 */
	public void aplicaAptidaoProporcional(Populacao populacao) {
		double soma = populacao.getSomaValorFuncao();
		for (int i = 0; i < populacao.getTamPopulacao(); i++) {
			float valorFirness = (float) (populacao.getIndividuos().get(i)
					.getValorFuncaoObjetivo() / soma);
			populacao.getIndividuos().get(i).setFitness(valorFirness);
		}
		// ordena na �rdem crescnte.
		Collections.sort(populacao.getIndividuos());

	}

	/**
	 * Gera aptid�o por rankeamento linear.
	 * 
	 * @param populacao
	 */
	public void aplicaAtidaoRankingLinear(Populacao populacao) {
		Collections.sort(populacao.getIndividuos());
		// if (populacao.getTamPopulacao() > 100) {
		// System.out.println("Teste");
		// }
		for (int i = 0; i < populacao.getTamPopulacao(); i++) {
			Individuo individuo = populacao.getIndividuos().get(i);
			int min = getMinimo();
			int max = getMaximo();
			int tamanhoPop = populacao.getTamPopulacao();
			float divisao = ((float) (tamanhoPop - i))
					/ ((float) (tamanhoPop - 1));
			float fitness = min + (max - min) * divisao;
			individuo.setFitness(fitness);
		}

	}

	/**
	 * Realiza Crossover e muta��o.
	 * 
	 * @param populacaoIntermediaria
	 * @return
	 */
	public Populacao realizaCrossoverMutacao(Populacao populacaoIntermediaria) {
		Populacao populacaoNova = new Populacao(20, getNumGenes(), false);
		Random r = new Random();
		for (int i = 0; i < populacaoIntermediaria.getTamPopulacao(); i += 2) {
			float valorRandom = r.nextFloat();
			if (valorRandom < getTaxaDeCrossover()) {
				int posicaoCorte = r.nextInt(8);

				// Pai 1 X1
				StringBuilder x1Pai1 = populacaoIntermediaria.getIndividuos()
						.get(i).getCromossomo().getGeneX1String();
				// Pai 2 X1
				StringBuilder x1Pai2 = populacaoIntermediaria.getIndividuos()
						.get(i + 1).getCromossomo().getGeneX1String();
				// Pai 1 X2
				StringBuilder x2Pai1 = populacaoIntermediaria.getIndividuos()
						.get(i).getCromossomo().getGeneX2String();
				// Pai 2 X2
				StringBuilder x2Pai2 = populacaoIntermediaria.getIndividuos()
						.get(i + 1).getCromossomo().getGeneX2String();

				// Cria X1 de filho 1.
				StringBuilder filho1X1 = new StringBuilder();
				filho1X1.append(x1Pai1.substring(0, posicaoCorte));
				filho1X1.append(x1Pai2.substring(posicaoCorte));
				// Cria X2 de filho 1.
				StringBuilder filho1X2 = new StringBuilder();
				filho1X2.append(x2Pai1.substring(0, posicaoCorte));
				filho1X2.append(x2Pai2.substring(posicaoCorte));

				// Cria X1 filho 2
				StringBuilder filho2X1 = new StringBuilder();
				filho2X1.append(x1Pai1.substring(posicaoCorte));
				filho2X1.append(x1Pai2.substring(0, posicaoCorte));
				// Cria X2 filho 2
				StringBuilder filho2X2 = new StringBuilder();
				filho2X2.append(x2Pai1.substring(posicaoCorte));
				filho2X2.append(x2Pai2.substring(0, posicaoCorte));
				// Realiza muta��o de gera cromossomo filho 1
				Cromossomo cromossomoFilho1 = realizaMutacao(filho1X1, filho1X2);
				// Realiza muta��o de gera cromossomo filho 2
				Cromossomo cromossomoFilho2 = realizaMutacao(filho2X1, filho2X2);
				// Gera indiv�duo 1
				Individuo individuoFilho1 = new Individuo(cromossomoFilho1);
				// Gera indiv�duo 2
				Individuo individuoFilho2 = new Individuo(cromossomoFilho2);
				// Adiciona indiv�duos filhos a popula��o nova.
				populacaoNova.getIndividuos().add(individuoFilho1);
				populacaoNova.getIndividuos().add(individuoFilho2);

			} else {
				populacaoNova.getIndividuos().add(
						populacaoIntermediaria.getIndividuos().get(i));
				populacaoNova.getIndividuos().add(
						populacaoIntermediaria.getIndividuos().get(i + 1));
			}

		}
		return populacaoNova;
	}

	/**
	 * Realiza Muta��o no filho
	 * 
	 * @param filho1x1
	 * @param filho1x2
	 * @return
	 */
	private Cromossomo realizaMutacao(StringBuilder filho1x1,
			StringBuilder filho1x2) {
		Random r = new Random();
		// Realiza muta��o do gene X1 do Cromossomo.
		for (int i = 0; i < getNumGenes(); i++) {
			float valorRandom = r.nextFloat();
			if (valorRandom < getTaxaDeMutacao()) {
				if (filho1x1.charAt(i) == '0') {
					filho1x1.setCharAt(i, '1');
				} else {
					filho1x1.setCharAt(i, '0');
				}
			}
		}
		// Realiza muta��o do gene X2 do Cromossomo.;
		for (int i = 0; i < getNumGenes(); i++) {
			float valorRandom = r.nextFloat();
			if (valorRandom < getTaxaDeMutacao()) {
				if (filho1x2.charAt(i) == '0') {
					filho1x2.setCharAt(i, '1');
				} else {
					filho1x2.setCharAt(i, '0');
				}
			}
		}
		// Gera cromossomo filho.
		Cromossomo cromossomo = new Cromossomo(filho1x1, filho1x2,
				getNumGenes());
		return cromossomo;

	}

	public static int getMaximo() {
		return maximo;
	}

	public static int getMinimo() {
		return minimo;
	}

	public double getTaxaDeCrossoverDefault() {
		return taxaDeCrossoverDefault;
	}

	public void setTaxaDeCrossoverDefault(double taxaDeCrossoverDefault) {
		this.taxaDeCrossoverDefault = taxaDeCrossoverDefault;
	}

	public double getTaxaDeMutacaoDefault() {
		return taxaDeMutacaoDefault;
	}

	public void setTaxaDeMutacaoDefault(double taxaDeMutacaoDefault) {
		this.taxaDeMutacaoDefault = taxaDeMutacaoDefault;
	}

	public int getNumGenes() {
		return numGenes;
	}

	public void setNumGenes(int numGenes) {
		this.numGenes = numGenes;
	}

	public int getNumMaxGeracoesDefault() {
		return numMaxGeracoesDefault;
	}

	public void setNumMaxGeracoesDefault(int numMaxGeracoesDefault) {
		this.numMaxGeracoesDefault = numMaxGeracoesDefault;
	}

	public int getTamPopDefault() {
		return tamPopDefault;
	}

	public void setTamPopDefault(int tamPopDefault) {
		this.tamPopDefault = tamPopDefault;
	}

	public double getTaxaDeCrossover() {
		return taxaDeCrossover;
	}

	public void setTaxaDeCrossover(double taxaDeCrossover) {
		this.taxaDeCrossover = taxaDeCrossover;
	}

	public double getTaxaDeMutacao() {
		return taxaDeMutacao;
	}

	public void setTaxaDeMutacao(double taxaDeMutacao) {
		this.taxaDeMutacao = taxaDeMutacao;
	}

	public int getTamPop() {
		return tamPop;
	}

	public void setTamPop(int tamPop) {
		this.tamPop = tamPop;
	}

	public int getNumMaxGeracoes() {
		return numMaxGeracoes;
	}

	public void setNumMaxGeracoes(int numMaxGeracoes) {
		this.numMaxGeracoes = numMaxGeracoes;
	}

}
