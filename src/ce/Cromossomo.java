package ce;

import java.util.ArrayList;
import java.util.Random;

public class Cromossomo {

	private ArrayList<Integer> geneX1;
	private ArrayList<Integer> geneX2;
	int geneX1Base10;
	int geneX2Base10;
	float geneX1Real;
	float geneX2Real;


	private StringBuilder geneX1String;
	private StringBuilder geneX2String;

	public Cromossomo(StringBuilder geneX1String, StringBuilder geneX2String,
			int tamanhoGene) {
		this.geneX1String = geneX1String;
		this.geneX2String = geneX2String;
		geneX1Real = Utils.calculaValorReal(geneX1Base10,
				Algoritimo.getMinimo(), Algoritimo.getMaximo(), tamanhoGene);
		geneX2Base10 = Utils.converteBinarioReal(geneX2String.toString());
		geneX2Real = Utils.calculaValorReal(geneX2Base10,
				Algoritimo.getMinimo(), Algoritimo.getMaximo(), tamanhoGene);
	}

	public Cromossomo(boolean isAleatorio, int tamanhoGene) {
		if (isAleatorio) {
			geneX1 = new ArrayList<Integer>();
			geneX2 = new ArrayList<Integer>();
			geneX1String = new StringBuilder();
			geneX2String = new StringBuilder();
			createRandomCromossomo(tamanhoGene);
			geneX1Base10 = Utils.converteBinarioReal(geneX1String.toString());
			geneX1Real = Utils.calculaValorReal(geneX1Base10,
					Algoritimo.getMinimo(), Algoritimo.getMaximo(), tamanhoGene);
			geneX2Base10 = Utils.converteBinarioReal(geneX2String.toString());
			geneX2Real = Utils.calculaValorReal(geneX2Base10,
					Algoritimo.getMinimo(), Algoritimo.getMaximo(), tamanhoGene);
		}
	}

	public void createRandomCromossomo(int bits) {
		Random random = new Random();
		for (int i = 0; i < bits; i++) {
			Integer genex1 = new Integer((random.nextBoolean()) ? 1 : 0);
			Integer genex2 = new Integer((random.nextBoolean()) ? 1 : 0);
			geneX1.add(genex1);
			geneX1String.append(genex1.toString());
			geneX2.add(genex2);
			geneX2String.append(genex2.toString());
		}
	}

	public ArrayList<Integer> getGeneX1() {
		return geneX1;
	}

	public void setGeneX1(ArrayList<Integer> geneX1) {
		this.geneX1 = geneX1;
	}

	public ArrayList<Integer> getGeneX2() {
		return geneX2;
	}

	public void setGeneX2(ArrayList<Integer> geneX2) {
		this.geneX2 = geneX2;
	}

	public int getGeneX1Base10() {
		return geneX1Base10;
	}

	public void setGeneX1Base10(int geneX1Base10) {
		this.geneX1Base10 = geneX1Base10;
	}

	public int getGeneX2Base10() {
		return geneX2Base10;
	}

	public void setGeneX2Base10(int geneX2Base10) {
		this.geneX2Base10 = geneX2Base10;
	}

	public float getGeneX1Real() {
		return geneX1Real;
	}

	public void setGeneX1Real(float geneX1Real) {
		this.geneX1Real = geneX1Real;
	}

	public float getGeneX2Real() {
		return geneX2Real;
	}

	public void setGeneX2Real(float geneX2Real) {
		this.geneX2Real = geneX2Real;
	}

	public StringBuilder getGeneX1String() {
		return geneX1String;
	}

	public void setGeneX1String(StringBuilder geneX1String) {
		this.geneX1String = geneX1String;
	}

	public StringBuilder getGeneX2String() {
		return geneX2String;
	}

	public void setGeneX2String(StringBuilder geneX2String) {
		this.geneX2String = geneX2String;
	}

}
