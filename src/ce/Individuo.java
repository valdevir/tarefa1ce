package ce;

public class Individuo implements Comparable<Individuo> {

	/**
	 * Corresponde ao conjunto de bin�rios (gene) que ir� reprresentar o valor
	 */
	private Cromossomo cromossomo;
	/**
	 * Valor de apdtid�o deste indiv�duo ou seja desta solu��o.
	 */
	private float fitness;

	/**
	 * Valor do cromossmo na base 10
	 */
	// private int ValorCromossomoBase10;

	/**
	 * Valor do indiv�duo na fun��o objetivo.
	 */
	private float valorFuncaoObjetivo;

	/**
	 * Valor individuo normatizado, dentre o intervalo proposto (-10, 10)
	 */
	// private float valorReal;

	/**
	 * 
	 * 
	 * @param numGenes
	 */
	public Individuo(boolean isAleatorio, int numGenesByCromossomo) {
		cromossomo = new Cromossomo(isAleatorio, numGenesByCromossomo);
	}

	public Individuo() {

	}

	public Individuo(Cromossomo cromossomo) {
		setCromossomo(cromossomo);
	}

	public Cromossomo getCromossomo() {
		return cromossomo;
	}

	public void setCromossomo(Cromossomo cromossomo) {
		this.cromossomo = cromossomo;
	}

	public float getFitness() {
		return fitness;
	}

	public void setFitness(float fitness) {
		this.fitness = fitness;
	}

	public float getValorFuncaoObjetivo() {
		return valorFuncaoObjetivo;
	}

	public void setValorFuncaoObjetivo(float valorFuncaoObjetico) {
		this.valorFuncaoObjetivo = valorFuncaoObjetico;
	}

	@Override
	public int compareTo(Individuo outroIndividuo) {
		if (this.valorFuncaoObjetivo < outroIndividuo.getValorFuncaoObjetivo()) {
			return -1;
		}
		if (this.fitness > outroIndividuo.getValorFuncaoObjetivo()) {
			return 1;
		}
		return 0;
	}

}
