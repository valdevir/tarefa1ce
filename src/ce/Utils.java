package ce;

public class Utils {

	public static float calculaValorReal(int valordecimal, int min, int max,
			int bits) {
		float valorReal = 0;
		// double bitsValor = 2 ^ bits;
		valorReal = (float) (min + (max - min) * valordecimal
				/ (Math.pow(2, bits) - 1));
		return valorReal;
	}

	public static int converteBinarioReal(String stringBinario) {
		int valorDecimal = Integer.parseInt(stringBinario, 2);
		return valorDecimal;
	}

}
